async function deleteToDo(value) {
            url = "/delete/" + value;
            console.log(url); 
            const response = await fetch(url, {
                method: "DELETE",
            })
            window.location.href = "/";
        }

async function complete(todo_id) {
    window.location.href = "/compile/" + todo_id;
}