import hashlib
from pathlib import Path

import app.db_models
from app.db_models import Image
from app.db_connect import SessionLocal
from app.shemas import CreateImage


def get_checksum(filename) -> str:
    """
    Function for calculating checksum (md5) of .zip archive
    :param archive_name: name of .zip archive
    :return: checksum (md5) of archive
    """
    hash_sum = hashlib.md5()
    path = (filename)
    with open(f"{path}", mode="rb") as arch:
        symbols = arch.read()
        hash_sum.update(symbols)
    return hash_sum.hexdigest()


def images():
    db = SessionLocal()
    images = db.query(app.db_models.Image).all()
    db.close()
    return images


def create_images(image: CreateImage):
    db = SessionLocal()
    check = db.query(app.db_models.Image).filter(app.db_models.Image.hashed_image == image.hashed_image).first()
    if check:
        return check
    im = Image(path=image.path,
               hashed_image=image.hashed_image)
    db.add(im)
    db.commit()
    db.refresh(im)
    db.close()
    return im

def get_image(id: int = None):
    db = SessionLocal()
    user = db.query(Image).filter(Image.id == id).first()
    db.close()
    return user