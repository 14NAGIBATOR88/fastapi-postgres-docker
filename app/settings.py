from pydantic import BaseSettings


# _________________for_deploying_in_docker_________________
class Settings(BaseSettings):
    postgres_server: str
    postgres_port: int
    postgres_user: str
    postgres_password: str
    postgres_db: str

    # class Settings(BaseSettings):
    #     postgres_server: str = "db"
    #     postgres_port: int = "5432"
    #     postgres_user: str = "postgres"
    #     postgres_password: str = "azerty123"
    #     postgres_db: str = "app"

    class Config:
        env_file = ".env"


settings = Settings()
