import time

from fastapi.testclient import TestClient

from main import server

client = TestClient(server)



data = {'username': 'a', 'password':'a'}
response = client.post("/register-user", data=data)
a = response.json()
response = client.post("/token", data=data)
token = str(response.json())
header = {"token": token}


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200


def test_delete_todo():
    response = client.post("/add",
                           data=dict(title='bar'),
                           headers=header
                           )
    assert response.status_code == 200
    response = client.delete("/delete/1",
                             headers=header
                             )
    assert response.status_code == 200
    response = client.delete("/delete/1",
                             headers=header
)
    assert response.status_code == 401


def test_import_xlsx():
    f = open("./todo.xlsx", "rb")
    data = f.read()
    files = {"file_upload": ("todo.xlsx", data)}
    response = client.post("/import",
                           files=files,
                           headers=header
                           )
    assert response.status_code == 200
    assert response.json() == {
        '1': ['rhbfjijbyijtypofprj', 'bkwvdjikdphaldmrdiq', 'empty', 'generated', False, '21-11-2023', None, None, ]}


def test_list_import_todo():
    response = client.get("/xlsx")
    assert response.status_code == 200
    assert response.json() == [{"filename": "todo.xlsx", "id": 1}]


def test_git_check():
    response = client.post("/git-create",
                           headers=header)
    assert response.status_code == 200
    assert response.json() == {
        "title": "asdsad",
        "details": "asd",
        "tag": "empty",
        "origin": "created",
        "completed": False,
        "completed_date": None,
        "created_date": "2023-11-19",
        "image_id": None,
        "owner_id": 1
    }


def test_list():
    response = client.get("/list",
                          headers=header)
    assert response.status_code == 200
    assert response.json() == [
        {
            "tag": "empty",
            "completed": False,
            "image_id": None,
            "origin": "created",
            "title": "asdsad",
            "id": 3,
            "details": "asd",
            "created_date": "2023-11-19",
            "completed_date": None,
            "owner_id": 1

        },
        {
            "tag": "empty",
            "completed": False,
            "image_id": None,
            "origin": "generated",
            "title": "rhbfjijbyijtypofprj",
            "id": 2,
            "details": "bkwvdjikdphaldmrdiq",
            "created_date": None,
            "completed_date": "21-11-2023",
            "owner_id": 1

        }
    ]


def test_edit():
    data = dict(title='edited', details="edited", tag="plans")
    response = client.post("/edit/2", data=data, headers=header)
    assert response.status_code == 200
    assert response.json() == {
        "id": 2,
        "title": "edited",
        "details": "edited",
        "tag": "plans",
        "completed": False,
        "image_id": None,
    }
    f = open("./el_che.jpeg", "rb")
    file = f.read()
    files = {"foto": ("el_che.jpeg", file)}
    response = client.post("/edit/2", data=data, files=files, headers=header)
    assert response.status_code == 200
    assert response.json() == {
        "id": 2,
        "title": "edited",
        "details": "edited",
        "tag": "plans",
        "completed": False,
        "image_id": 1,
    }
