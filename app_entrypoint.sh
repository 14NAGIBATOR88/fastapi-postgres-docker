#!/bin/bash
if [ "$DEBUG" = "test" ]
then
  echo "Running tests"
  python -m pytest -vv -p no:warnings
  #-p для чтение директории app(он не видит ее без этого флага)
  #-vv для полного чтения ошибки
else
  echo "Running in production mode"
  uvicorn app.main:server --host=0.0.0.0 --reload
fi