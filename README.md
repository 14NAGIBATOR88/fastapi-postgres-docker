

Для запуска (автообновляемый контейнер)
```
docker compose up --build
```


Для запуска тестов
```
env DEBUG=test docker compose up --build
```

все урлы можно глянуть тут http://localhost:8000/docs


для очистки бд
```
docker compose down -v
```

шоб снести все контейнры полностью
```
docker system prune --all --volumes 
```



